#!/usr/bin/env python

from . import tsto, dlc
import sys, cmdln, logging, getpass,time

class TSTO_CMD(cmdln.Cmdln):
	def __init__(self):
		cmdln.Cmdln.__init__(self)
		self.prompt = "tsto> "
		self.dlc = dlc.get_latest()
		self.tsto = None
		self.error = False

		self.items = []
		for itemType in tsto.itemTypes.values():
			for item in self.dlc.xml.findall(".//%s[@name]"%itemType):
				self.items.append(item.get("name"))

		self.spendables = ["Money","Donuts"]
		for spendable in self.dlc.xml.findall(".//SpendableCurrency[@name]"):
			self.spendables.append(spendable.get("name"))

		self.characters = []
		for character in self.dlc.xml.xpath(".//Character[@name and @id and (Group or Set or UnlockSound or Cost)]"):
			self.characters.append(character.get("name"))

	def do_login(self, subcmd, opts, email=None, password=None):
		"""${cmd_name}: log into your origin account

		If neither email nor password is given, the last used account will be logged in.

		${cmd_usage}
		${cmd_option_list}
		"""
		self.tsto = tsto.TSTO(email,password)
		if self.tsto.displayName is not None:
			self.prompt = "%s> " % self.tsto.displayName
			self.tsto.download()

	def do_logout(self, subcmd, opts):
		if self.tsto.LandMessage.id != '':
			answer = raw_input("Do you want to upload your changes? [y|N] ")
			if answer in ["y","Y"]:
				self.tsto.upload()
		del self.tsto
		self.tsto = None
		self.prompt = "tsto> "

	def do_upload(self, subcmd, opts):
		"""${cmd_name}: Upload changes to EA-Server"""
		self.tsto.upload()

	def do_download(self,subcmd,opts):
		self.tsto.download()

# SPENDABLES
	@cmdln.option("-a","--all",action="store_true",help="show all spendables (if not set only spendables used in account will be shown)")
	def do_spendables(self, subcmd, opts):
		"""${cmd_name}: list spendables

		${cmd_option_list}
		"""
		donuts = self.tsto.loadCurrency()
		rf = "{:<30} {:>12}"
		print(rf.format("Donuts", donuts.Balance))
		print(rf.format("Money", self.tsto.LandMessage.userData.money))
		for stype in self.tsto.spendables.keys():
			name = self.dlc.xml.find(".//SpendableCurrency[@Type='%d']"%stype).get("name")
			print(rf.format(name, self.tsto.spendables[stype].amount))
		if opts.all == True:
			for spendable in self.dlc.xml.findall(".//SpendableCurrency[@name]"):
				if int(spendable.get("Type")) in self.tsto.spendables.keys():
					continue
				else:
					print(rf.format(spendable.get("name"),''))

	def do_spendablesSet(self, subcmd, opts, types, amount):
		"""${cmd_name}: set spendable amount

			spendablesSet [TYPE[,TYPE]...] AMOUNT
		"""
		stypes = [str(x) for x in types.split(',')]
		amount = int(amount)
		add = subcmd == "spendablesAdd"

		for st in stypes:
			if st == "Donuts":
				if not add:
					donuts = self.tsto.loadCurrency()
					if amount < donuts.Balance:
						logging.error("Reducing the amount of donuts is not supported. You'll have let Homer eat them.")
					else:
						self.tsto.donutsAdd(amount - donuts.Balance)
				else:
					self.tsto.donutsAdd(amount)
				continue
			elif st == "Money":
				if add:
					self.tsto.LandMessage.userData.money += amount
				else:
					self.tsto.LandMessage.userData.money = amount
				continue

			try:
				t = int(self.dlc.xml.find(".//SpendableCurrency[@name='%s'][@Type]"%st).get("Type"))
			except KeyError as e:
				logging.error("Unknown spendable '%s'" % st)
				continue
			except TypeError as e:
				logging.error("Can't find type id for '%s'" % st)
				continue


			if t in self.tsto.spendables.keys():
				if add:
					self.tsto.spendables[t].amount += amount
				else:
					self.tsto.spendables[t].amount = amount
				#TODO replace this loop
				for s in self.tsto.LandMessage.friendData.spendable:
					if s.type == t:
						s.amount = self.tsto.spendables[t].amount
			else:
				sp = self.tsto.LandMessage.spendablesData.spendable.add()
				sp.type = t
				sp.amount = amount
				self.tsto.loadSpendables()

	def complete_spendablesSet(self,text,line,begidx,endidx):
		l = line.split(" ")
		if endidx <= len(l[0]) + len(l[1]) + 1:
			return [s for s in self.spendables if s.startswith(text)]
		else:
			return []

	def do_spendablesAdd(self, subcmd, opts, types, amount):
		"""${cmd_name}: add spendable amount

			spendablesAdd [TYPE[,TYPE]...] AMOUNT
		"""
		self.do_spendablesSet(subcmd, opts, types, amount)

	def complete_spendablesAdd(self,*args):
		return self.complete_spendablesSet(*args)

# INVENTORY
	def do_inventory(self,subcmd,opts):
		"""List inventory"""
		rf = "{:>3} x {:<30}"
		for item in self.tsto.LandMessage.inventoryItemData:
			try:
				name = self.dlc.xml.find(".//%s[@id='%d'][@name]"%(tsto.itemTypes[item.itemType],int(item.itemID))).get("name")
				print(rf.format(item.count,name))
			except AttributeError as e:
				print("Could not find item (type: %d, ID: %d, count: %d)"%(item.itemType,item.itemID,item.count))
				self.error = True

	def do_inventoryAdd(self,subcmd,opts,items,count=1):
		"""${cmd_name}: add items to inventory

			${cmd_name} [ITEM[,ITEM]...] COUNT

			You can use tab-completion for item names.
		"""
		itemNames = [str(x) for x in items.split(',')]
		count = int(count)
		add = subcmd == "inventoryAdd"

		for itemName in itemNames:
			for itemType in tsto.itemTypes.keys():
				#TODO check for uniqueness
				#TODO probably in init ownedBuildings = [b.building for b in t.LandMessage.buildingData]
				item = self.dlc.xml.find(".//%s[@name='%s'][@id]"%(tsto.itemTypes[itemType],itemName))
				if item is None:
					continue
				itemID = int(item.get("id"))
				self.tsto.inventoryAdd(itemID,itemType,count,add)
				break
			else:
				logging.error("item %s not found"%itemName)

	def complete_inventoryAdd(self,text,line,begidx,endidx):
		l = line.split(" ")
		if endidx <= len(l[0]) + len(l[1]) + 1:
			return [s for s in self.items if s.startswith(text)]
		else:
			return []

	def do_inventorySet(self,subcmd,opts,items,count):
		"""${cmd_name}: set itemcount in inventory

			${cmd_name} [ITEM[,ITEM]...] COUNT

			You can use tab-completion for items in inventory.
		"""
		self.do_inventoryAdd(subcmd,opts,items,count)

	def complete_inventorySet(self,text,line,begidx,endidx):
		"""Only show items in inventory"""
		l = line.split(" ")
		if endidx <= len(l[0]) + len(l[1]) + 1:
			items = []
			for item in self.tsto.LandMessage.inventoryItemData:
				items.append(self.dlc.xml.find(".//%s[@id='%d']"%(tsto.itemTypes[item.itemType],item.itemID)).get("name"))
			return [s for s in items if s.startswith(text)]
		else:
			return []

	def do_characters(self,subcmd,opts):
		for char in self.tsto.LandMessage.characterData:
			character = self.dlc.xml.find(".//Character[@name][@id='%s']"%char.character)
			if character is not None:
				print(character.get("name"))
			else:
				logging.error("Could not find name for character %s"%char.character)

	def do_charactersAdd(self,subcmd,opts,characters):
		"""${cmd_name}: add characters

			${cmd_name} [CHARACTER[,CHARACTER]...] COUNT

			You can use tab-completion for character names.
		"""
		characterNames = [str(x) for x in characters.split(',')]

		for characterName in characterNames:
			character = self.dlc.xml.find(".//Character[@name='%s'][@id]"%characterName)
			if character is None:
				logging.error("character %s not found"%characterName)
				continue
			characterID = int(character.get("id"))
			#TODO skind,subland
			self.tsto.characterAdd(characterID)

	def complete_charactersAdd(self,text,line,begidx,endidx):
		l = line.split(" ")
		if endidx <= len(l[0]) + len(l[1]) + 1:
			return [s for s in self.characters if s.startswith(text)]
		else:
			return []
# BACKUPS
	def do_backups(self,subcmd,opts):
		"""List backups"""
		rf = "{:<27} {:<30}"
		for backup in self.tsto.backups():
			print(rf.format(time.ctime(backup["date"]),backup["file"]))

	def do_loadBackup(self,subcmd,opts,backup,extra=False):
		self.tsto.loadFile(backup,extra)
	def do_saveBackup(self,subcmd,opts,filename,extra=False):
		self.tsto.saveFile(backup,extra)
# MISC
	def do_hurry(self, subcmd, opts):
		"""${cmd_name}: finish all jobs"""
		for job in self.tsto.LandMessage.jobData:
			job.state = 2

	def do_quests(self, subcmd, opts):
		"""${cmd_name}: show quests"""
		rf = "{:>5} {:>10} {:>10} {:<20}"
		print(rf.format("state","completed", "objectives" ,"questname"))
		for q in self.tsto.LandMessage.questData:
			if q.numObjectives > 0:
				name = self.dlc.xml.find(".//Quest[@id='%d'][@name]"%q.questID).get("name")
				print(rf.format(q.questState, q.timesCompleted, q.numObjectives, name or q.questID))

	def do_questsComplete(self, subcmd, opts, quests):
		"""${cmd_name}: complete quests

			${cmd_name} [QUEST[,QUEST]...]

			You can use tab-completion for started quests.
		"""
		for q in quests.split(","):
			quest = self.dlc.xml.find(".//Quest[@name='%s'][@id]"%q)
			if quest == None:
				logging.error("Could not find quest '%s'"%q)
				continue
			else:
				if not self.tsto.questComplete(int(quest.get("id"))):
					logging.error("Error completing '%s'"%q)

	def complete_questsComplete(self,text,line,begidx,endidx):
		l = line.split(" ")
		if endidx <= len(l[0]) + len(l[1]) + 1:
			quests = []
			for q in self.tsto.LandMessage.questData:
				if q.numObjectives > 0:
					quests.append(self.dlc.xml.find(".//Quest[@id='%d'][@name]"%q.questID).get("name"))
			return [s for s in quests if s.startswith(text)]
		else:
			return []

	def do_cleanup(self,subcmd,opts):
		"""Clean debris"""
		# let's find all the debris :)
		debris = []
		debrisnames = []
		for building in self.dlc.xml.findall(".//Building[@name][@id]"):
			if building.get("name").lower().find("debris")>-1:
				debris.append(int(building.get("id")))

		idx2del = []
		for idx, b in enumerate(self.tsto.LandMessage.buildingData):
			if b.building in debris:
				del self.tsto.LandMessage.buildingData[idx]

	def do_showInfo(self,subcmd,opts):
		"""Show short info"""
		rf = "{:<6} {:>12}"
		donuts = self.tsto.loadCurrency()
		print(rf.format("Level", self.tsto.LandMessage.userData.level))
		print(rf.format("Donuts", donuts.Balance))
		print(rf.format("Money",self.tsto.LandMessage.userData.money))

# CMDLN stuff
	def precmd(self,line):
		if self.tsto is None and line[0] not in ["login","help","logout","EOF"]:
			print("You have to log in before using the command %s"%line[0])
			return False
		return cmdln.Cmdln.precmd(self, line)

	def postcmd(self,argv):
		if self.error:
			print("Something went wrong! Use 'python -im tsto.tsto' to manually repair things!")
			#TODO add backup-restore and ask user to restore a backup
		return cmdln.Cmdln.postcmd(self, argv)

	def preloop(self):
		self.optparser = self.get_optparser()	# should get fixed upstream soon https://github.com/trentm/cmdln/issues/37
		email = raw_input("Email: ")
		passwd = getpass.getpass()
		if email == "": email=None
		if passwd == "": passwd=None
		self.do_login("preloop", [], email, passwd)

	def postloop(self):
		self.do_logout("logout", [])
		print("Bye.")

if __name__ == '__main__':
	logging.basicConfig(level=logging.DEBUG)
	sys.exit( TSTO_CMD().cmdloop() )
