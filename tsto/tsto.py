#!/usr/bin/python
"""
TSTO tool.
WARNING: absolutly no warranties. Use this script at own risk.
Based on the tool from Oleg Polivets (jsbot@ya.ru) https://github.com/schdub/tsto
"""

from . import ld_pb2, CONFIGDIR
import sys, os, logging, requests, json, time, StringIO, gzip, collections, google.protobuf.message, random, struct
from stat import ST_CTIME

DESCRIPTION = 'The Simpsons Tapped Out tool'
URL_SIMPSONS = 'prod.simpsons-ea.com'
URL_OFRIENDS = 'm.friends.dm.origin.com'
URL_AVATAR = 'm.avatar.dm.origin.com'
URL_TNTAUTH = 'auth.tnt-ea.com'
URL_TNTNUCLEUS = 'nucleus.tnt-ea.com'
CT_PROTOBUF = 'application/x-protobuf'
CT_JSON = 'application/json'
CT_XML = 'application/xaml+xml'
VERSION_APP = '4.18.6'
VERSION_LAND = '35'

itemTypes = {0:"Building",1:"Character",2:"Consumable"}

class TSTO(object):
    class LoginError(Exception):
        def __init__(self, arg):
            self.strerror = ""
            self.args = set()
            if type(arg) == dict:
                self.args = [ x for x in arg.values() if x is not None ]
            else:
                self.strerror = str(arg)
                self.args = {arg}

    dataVersion = int(VERSION_LAND)

    headers = dict()
    headers["Accept"] = "*/*"
    headers["Accept-Encoding"] = "gzip"
    headers["client_version"] = VERSION_APP
    headers["server_api_version"] = "4.0.0"
    headers["EA-SELL-ID"] = "857120"
    headers["platform"] = "android"
    headers["os_version"] = "15.0.0"
    headers["hw_model_id"] = "0 0.0"
    headers["data_param_1"] = "2633815347"

    tokenPath = os.path.join(CONFIGDIR,'tokens')
    backupPath = os.path.join(CONFIGDIR,'backups')

    def __init__(self, email=None, password=None, token=True):
        """Login to origin account

        If token is True and password or email and password are missing
        we will try to use a saved token to login.

        After initialisation you should call download before trying to do stuff"""

        if not os.path.isdir(self.tokenPath):
            os.mkdir(self.tokenPath)
        if not os.path.isdir(self.backupPath):
            os.mkdir(self.backupPath)

        self.LandMessage = ld_pb2.LandMessage()
        self.ExtraLandMessage = None
        self.session = requests.Session()
        self.authSession = requests.Session()

        self.uid = None
        self.token = None
        self.encryptedToken = None
        self.updateToken = None
        self.displayName = None

        self.currency = None
        self.spendables = collections.OrderedDict()

        self.__login(email, password, token)

    def _request(self, method, content_type, host, path, keep_alive=True, data=[], uncomressedLen=-1):
        """Convenience wrapper around requests module"""
        url = ("https://%s%s" % (host, path)).encode('utf-8')

        # set headers
        headers = self.headers.copy()
        if uncomressedLen > -1:
            headers["Content-Encoding"] = "gzip"
            headers["Uncompressed-Length"] = uncomressedLen
            headers["Content-Length"] = len(data)
        if keep_alive == True:
            headers["Connection"] = "Keep-Alive"
            ssn = self.session if host == URL_SIMPSONS else self.authSession
        else:
            headers["Connection"] = "Close"
            ssn = requests
        headers["Content-Type"] = content_type

        r = ssn.request(method=method, url=url, headers=headers, verify=False, data=data)

        if r.headers['Content-Type'] == 'application/x-protobuf':
            logging.debug(r.headers['Content-Type'])
        else:
            logging.debug(r.content)

        if content_type == CT_JSON:
            return json.JSONDecoder().decode(r.content)
        else:
            return r.content

# AUTHENTICATION
    def __del__(self):
        """Log out"""
        if self.updateToken is None:
            return
        dtr = ld_pb2.DeleteTokenRequest()
        dtr.token = self.updateToken
        data = self._request("POST", CT_PROTOBUF, URL_SIMPSONS,
                "/mh/games/bg_gameserver_plugin/deleteToken/%s/protoWholeLandToken/" % (self.uid), data = dtr.SerializeToString())
        dtr = ld_pb2.DeleteTokenResponse()
        dtr.ParseFromString(data)
        if not dtr.result:
            raise TSTO.LoginError("Error logging out")

    def __login(self, email=None, password=None, token=True):

# GOD DAMN is this login procedure long...

        if password is not None:
            eToken = "%s/%s" % (email, password)
        elif token:
            if not self.__tokenLoad(email):
                raise TSTO.LoginError("Could not find token %s" % ("for %s" % email if email is not None else ""))

            data = self._request("POST", CT_JSON, URL_TNTNUCLEUS,
                    "/rest/token/validate", data = self.token)

            eToken = self.encryptedToken
        else:
            raise TSTO.LoginError("No login data available")

        data = self._request("POST", CT_JSON, URL_TNTNUCLEUS,
                "/rest/token/%s/" % (eToken))

        if "nucleusError" in data:
            raise TSTO.LoginError(data["nucleusError"]["failure"])

        self.displayName = data["displayName"]
        self.encryptedToken = data["encryptedToken"]

        self.token = data["token"]
        self.headers["nucleus_token"] = self.token
        self.headers["AuthToken"] = self.token

#TODO add exceptions for tntauth errors (do they happen?)
        # TNTAUTH
        data = self._request("GET", CT_JSON, URL_TNTAUTH,
                "/rest/oauth/origin/%s/Simpsons-Tapped-Out/" % self.token)
        self.headers["mh_auth_method"] = "tnt"
        self.headers["mh_auth_params"] = data["code"]
        self.headers["mh_client_version"] = "Android." + VERSION_APP
        tntId = data["tntId"]
        # get sessionkey and uid
        data = self._request("PUT", CT_PROTOBUF, URL_SIMPSONS,
                "/mh/users?appVer=2.2.0&appLang=en&application=tnt&applicationUserId=%s" % tntId)
        urm = ld_pb2.UsersResponseMessage()
        urm.ParseFromString(data)
        self.uid = urm.user.userId
        self.headers["mh_uid"] = self.uid
        self.headers["mh_session_key"] = urm.token.sessionKey

        # get update key
        data = self._request("GET", CT_PROTOBUF, URL_SIMPSONS,
                "/mh/games/bg_gameserver_plugin/checkToken/%s/protoWholeLandToken/" % (self.uid))
        wltr = ld_pb2.WholeLandTokenRequest()
        try:
            wltr.ParseFromString(data)
        except google.protobuf.message.DecodeError:
            wltr = ld_pb2.WholeLandTokenRequest()
            wltr.requestId = tntId
            data = wltr.SerializeToString()
            data = self._request("POST", CT_PROTOBUF, URL_SIMPSONS,
                    "/mh/games/bg_gameserver_plugin/protoWholeLandToken/%s/" % self.uid, True, data)
            wltr = ld_pb2.WholeLandTokenRequest()
        wltr.ParseFromString(data)
        self.updateToken = wltr.requestId
        self.headers["target_land_id"] = self.uid
        self.headers["land-update-token"] = self.updateToken

        if token and email is not None:
            self.__tokenStore(email)
        print("Logged in")


    def inventoryAdd(self, itemID, itemType=0, count=1,add=True):
        for idx in range(len(self.LandMessage.inventoryItemData)):
            item = self.LandMessage.inventoryItemData[idx]
            if item.itemID == itemID and item.itemType == itemType:
                if add:
                    item.count += count
                else:
                    if count <= 0:
                        logging.info("deleting %d"%itemID)
                        del self.LandMessage.inventoryItemData[idx]
                    else:
                        item.count = count
                break
        else:
            if count <= 0:
                logging.warn("item %d not in inventory"%itemID)
                return
            item = self.LandMessage.inventoryItemData.add()
            item.header.id = self.LandMessage.innerLandData.nextInstanceID
            item.itemID = itemID
            item.itemType = itemType
            item.count = count
            item.isOwnerList = False
            item.fromLand = 0
            item.sourceLen = 0
            self.LandMessage.innerLandData.nextInstanceID = item.header.id + 1
        self.LandMessage.innerLandData.numInventoryItems = len(self.LandMessage.inventoryItemData)

    def characterAdd(self,characterID,skin=0,subLandID=1):
        # dont add already existing characters!
        for char in self.LandMessage.characterData:
            if char.character == characterID:
                logging.warn("character %d already exists"%characterID)
                return
        c = self.LandMessage.characterData.add()
        c.character = characterID
        c.subLandID = subLandID
        c.skin = skin
        c.header.id=self.LandMessage.innerLandData.nextInstanceID
        self.LandMessage.innerLandData.nextInstanceID += 1
        self.LandMessage.innerLandData.numChars = len(self.LandMessage.characterData)

    def characterDelete(self,characterID):
        """DO NOT USE!"""
        for idx in range(len(self.LandMessage.characterData)):
            char = self.LandMessage.characterData[idx]
            if char.character == characterID:
                del self.LandMessage.characterData[idx]
                return True
        logging.warn("Character %d not found"%characterID)

# TOKEN
    def __tokenStore(self,email):
        with open(os.path.join(self.tokenPath, email), 'w') as f:
            f.write(self.token + '\n')
            f.write(self.encryptedToken + '\n')
            f.write(self.uid + '\n')

    def __tokenLoad(self, email=None):
        if email is None:
            if len(os.listdir(self.tokenPath))>0:
                email = max(os.listdir(self.tokenPath), key=lambda x: os.stat(os.path.join(self.tokenPath, x)).st_mtime)
            else:
                logging.warn("no tokens found")
                return False

        path = os.path.join(self.tokenPath, email)

        if os.path.isfile(path):
            content = list()
            with open(path, 'r') as f:
                content = [x.strip('\n') for x in f.readlines()]
            if len(content) >= 3:
                self.token = content[0]
                self.encryptedToken = content[1]
                self.uid = content[2]
                return True
            else:
                logging.warn("Unknown content in '%s'" % path)
        else:
            logging.warn("No such file or directory '%s'" % path)
        return False

# DOWN- / UPLOAD
    def download(self):
        """Download LandMessage."""
        data = self._request("GET", CT_PROTOBUF, URL_SIMPSONS,
                "/mh/games/bg_gameserver_plugin/protoland/%s/" % self.uid)
        self.LandMessage = ld_pb2.LandMessage()
        self.LandMessage.ParseFromString(data)

        # load stuff
        self.spendables = self.loadSpendables()

        # make backup
        self.saveFile("%s.%f" % (self.uid, time.time()))

    def upload(self):
        """Upload changes"""
        if self.LandMessage.id == '':
            logging.warn("no landmessage to upload")
            return
        # store last played time and send GZipped Land itself
        self.LandMessage.friendData.lastPlayedTime = int(time.time())
        data = self.LandMessage.SerializeToString()
        uncomressedLen = len(data)
        out = StringIO.StringIO()
        g = gzip.GzipFile(fileobj=out, mode="w")
        g.write(data)
        g.close()
        data = out.getvalue()
        data = self._request("POST", CT_PROTOBUF, URL_SIMPSONS,
                "/mh/games/bg_gameserver_plugin/protoland/%s/" % self.uid, True, data, uncomressedLen)

        msg = self.ExtraLandMessage
        if msg == None:
            return
        data = msg.SerializeToString()
        data = self._request("POST", CT_PROTOBUF, URL_SIMPSONS,
                "/mh/games/bg_gameserver_plugin/extraLandUpdate/%s/protoland/" % self.uid, True, data)
        self.ExtraLandMessage = None

# SPENDABLES
    def loadSpendables(self):
        """DEPRECATED! will be removed soon.
        Returns dict of spendables with type as key"""
        spendables = collections.OrderedDict()
        for spendable in self.LandMessage.spendablesData.spendable:
            spendables[spendable.type] = spendable
        self.spendables = spendables
        return spendables

    def loadCurrency(self):
        """Returns currencyData (currency means donuts)
         _ _,---._
       ,-','       `-.___
      /-;'               `._
     /\/          ._   _,'o \
    ( /\       _,--'\,','"`. )
     |\      ,'o     \'    //\
     |      \        /   ,--'""`-.
     :       \_    _/ ,-'         `-._
      \        `--'  /                )
       `.  \`._    ,'     ________,','
         .--`     ,'  ,--` __\___,;'
          \`.,-- ,' ,`_)--'  /`.,'
           \( ;  | | )      (`-/
             `--'| |)       |-/
               | | |        | |
               | | |,.,-.   | |_
               | `./ /   )---`  )
              _|  /    ,',   ,-'
 hrr donuts  ,'|_(    /-<._,' |--,
             |    `--'---.     \/ \
             |          / \    /\  \
           ,-^---._     |  \  /  \  \
        ,-'        \----'   \/    \--`.
       /            \              \   \
        """
        if self.currency is not None:
            return self.currency
        data = self._request("GET", CT_PROTOBUF, URL_SIMPSONS,
                "/mh/games/bg_gameserver_plugin/protocurrency/%s/" % self.uid)
        currdat = ld_pb2.CurrencyData()
        currdat.ParseFromString(data)
        self.currency = currdat
        return currdat

    def donutsAdd(self, amount):
        if self.ExtraLandMessage == None:
            self.ExtraLandMessage = ld_pb2.ExtraLandMessage()
        elm = self.ExtraLandMessage
        nextId = self.LandMessage.innerLandData.nextCurrencyID
        sum = 0
        while sum < amount:
            cur = random.randint(499, 500)
            if sum + cur > amount:
                cur = amount - sum
            delta = elm.currencyDelta.add()
            delta.id = nextId
            delta.reason = "JOB"
            delta.amount = cur
            nextId += 1
            sum += cur
        self.LandMessage.innerLandData.nextCurrencyID = nextId
        self.currency.Balance += amount

    def questComplete(self,qid):
        qst = None
        for qst in self.LandMessage.questData:
            if qst.questID == qid:
                qst.questState = 5
                qst.numObjectives = 0
                qst.questScriptState = 0
                qst.timesCompleted += 1
                # delete objective data
                for i in reversed(range(len(qst.objectiveData))):
                    del qst.objectiveData[i]
                return True
        return False

# BACKUPS
    def saveFile(self,filename,extra=False):
        """Save LandMessage or ExtraLandMessage to file"""
        if extra:
            msg = self.ExtraLandMessage
        else:
            msg = self.LandMessage
        data = msg.SerializeToString()
        with open(os.path.join(self.backupPath,filename),'wb') as f:
            f.write(struct.pack('i', int(time.time())))
            f.write(struct.pack('i', 0))
            f.write(struct.pack('i', len(data)))
            f.write(data)

    def loadFile(self,filename,extra=False):
        """Load LandMessage or ExtraLandMessage from file"""
        data = ''
        with open(os.path.join(self.backupPath,filename),'rb') as f:
            f.seek(0x0c)
            data = f.read()
        if extra:
            self.ExtraLandMessage = ld_pb2.ExtraLandMessage().ParseFromString(data)
        else:
            self.LandMessage = ld_pb2.LandMessage().ParseFromString(data)

    def backups(self):
        if self.uid == None:
            raise TypeError("ERR: I don't know your mayhem ID. Login first.")
        backups = []
        for fn in os.listdir(self.backupPath):
            if fn.startswith(self.uid + '.'):
                date = os.stat(os.path.join(self.backupPath,fn))[ST_CTIME]
                backups.append({"date":date,"file":os.path.basename(fn)})
        return backups

if __name__ == "__main__":
    t=TSTO()
